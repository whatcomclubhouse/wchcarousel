<?php
/**
 * carousel - the carousel component to be placed on the homepage
 * 
 * Copyright 2022 Whatcom Clubhouse
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
require_once ABSPATH."/wp-includes/post.php";

function emit_carousel(): string
{
    $images = wchcarousel_carousel_images_get();

    $has_active = false;
    ?>
    <div class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
    <?php
            foreach ($images as $image) {
                $image_url = wp_get_attachment_url($image->attachment_id);
    ?>
                <div class="carousel-item <?php echo $has_active ? "" : "active"; ?>">
                    <img src="<?php echo $image_url; ?>" class="d-block w-100" alt="">
                    <div class="carousel-caption d-block">
                        <p><?php echo $image->caption; ?></p>
                    </div>
                </div>
    <?php
                $has_active = true;
            }
    ?>
        </div>
    </div>
    <?php
}

echo emit_carousel();

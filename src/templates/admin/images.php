<?php
/**
 * images - the image selector form
 * 
 * Copyright 2022 Whatcom Clubhouse
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
require_once ABSPATH."/wp-includes/l10n.php";

global $wchcarousel_text_domain;

/**
 * @var bool $is_active whether the carousel is currently active
 */
$is_active = wchcarousel_get_is_active();

/**
 * @var int $image_count the number of images in the carousel
 */
$image_count = wchcarousel_get_image_count();
?>
<h2 class="mt-3"><?php _e("Homepage Carousel Images", $wchcarousel_text_domain); ?></h2>
<form class="form-floating" id="wchcarousel_admin_form" method="post" action="admin-post.php">
    <input class="d-none" name="action" value="image_submit"/>
    <div class="row mb-3">
        <label for="wchcarousel_admin_is_active" class="col-sm-2 col-form-label">Active:</label>
        <div class="col-sm-10">
            <input id="wchcarousel_admin_is_active" class="form-control" type="checkbox" name="is_active" value="true" <?php echo $is_active ? "checked" : ""; ?>/>
        </div>
    </div>
    <div class="row mb-3">
        <label for="wchcarousel_admin_image_count" class="col-sm-2 col-form-label">Number of Images:</label>
        <div class="col-sm-10">
            <input id="wchcarousel_admin_image_count" class="form-control" type="number" value="<?php echo $image_count; ?>"/>
        </div>
    </div>
    <div id="wchcarousel_admin_images" class="mb-3">
        Loading...
    </div>
    <button type="submit" class="btn btn-primary btn-sm">Save</button>
</form>

<?php 
/**
 * Plugin Name: WchCarousel
 * Plugin URI: https://gitlab.com/whatcomclubhouse/wchcarousel
 * Description: A home-page carousel plugin for WordPress
 * Version: 0.9.0
 * Requires at least: 3.5.0
 * Author: Whatcom Clubhouse
 * Author URI: https://whatcomclubhouse.org
 * License: MIT
 * License URI: https://opensource.org/licenses/MIT
 * Text Domain: wchcarousel
 */
require_once ABSPATH."/wp-includes/option.php";
require_once ABSPATH."/wp-includes/plugin.php";

require_once ABSPATH."/wp-includes/functions.wp-scripts.php";
require_once ABSPATH."/wp-includes/functions.wp-styles.php";

require_once ABSPATH."/wp-admin/includes/plugin.php";

/**
 * The relevant components of a WordPress image attachment
 */
class wchcarousel_wpimage {
   public $attachment_id;
   public $name;
   public $url;

   function __construct(int $attachment_id, string $name, string $url)
   {
      $this->attachment_id = $attachment_id;
      $this->name = $name;
      $this->url = $url;
   }
}

/**
 * A single carousel image
 */
class wchcarousel_image {
   public $attachment_id;
   public $caption;

   function __construct(int $attachment_id, string $caption)
   {
      $this->attachment_id = $attachment_id;
      $this->caption = $caption;
   }
}

/**
 * @var string $wchcarousel_version the plugin version
 */
$wchcarousel_version = "0.9.0";

/**
 * @var string $wchcarousel_text_domain the text domain
 */
$wchcarousel_text_domain = "wchcarousel";

if (!function_exists("wchcarousel_activate")) {
   /**
    * Activate the plugin
    *
    * @return void
    */
   function wchcarousel_activate(): void
   {
      add_option("wchcarousel_active");
      add_option("wchcarousel_list");
   }
}

if (!function_exists("wchcarousel_deactivate")) {
   /**
    * Deactivate the plugin
    *
    * @return void
    */
   function wchcarousel_deactivate(): void
   { }
}

if (!function_exists("wchcarousel_uninstall")) {
   /**
    * Uninstall the plugin
    *
    * @return void
    */
   function wchcarousel_uninstall(): void
   {
      delete_option("wchcarousel_active");
      delete_option("wchcarousel_list");
   }
}

if (!function_exists("wchcarousel_styles")) {
   /**
    * Enqueue styles.
    *
    * @return void
    */
   function wchcarousel_styles(): void
   {
      global $wchcarousel_version;

      wp_register_style("wchcarousel-style", plugin_dir_url(__FILE__)."assets/css/index.css", array(), $wchcarousel_version);
      wp_enqueue_style("wchcarousel-style");
   }
}

if (!function_exists("wchcarousel_scripts")) {
   /**
    * Enqueue scripts.
    *
    * @return void
    */
   function wchcarousel_scripts(): void
   {
      global $wchcarousel_version;

      wp_register_script("wchcarousel-script", plugin_dir_url(__FILE__)."assets/js/index.min.js", array(), $wchcarousel_version, true);
      wp_enqueue_script("wchcarousel-script");
   }
}

if (!function_exists("wchcarousel_admin_menu_images")) {
   /**
    * Images selector admin menu page
    *
    * @return void
    */
   function wchcarousel_admin_menu_images(): void
   {
      include plugin_dir_path(__FILE__)."templates/admin/images.php";
   }
}

if (!function_exists("wchcarousel_admin_menu")) {
   /**
    * Admin menu callback
    *
    * @return void
    */
   function wchcarousel_admin_menu(): void
   { 
      add_menu_page(
         "WCH Carousel",
         "WCH Carousel",
         "manage_options",
         "wchcarousel_admin_menu",
         "wchcarousel_admin_menu_images",
         "dashicons-book-alt"
      );

      add_submenu_page(
         "wchcarousel_admin_menu",
         "Images",
         "Images",
         "manage_options",
         "wchcarousel_admin_menu_images",
         "wchcarousel_admin_menu_images"
      );
   }
}

if (!function_exists("wchcarousel_get_is_active")) {
   /**
    * Determine the active state of the carousel
    *
    * @return bool if the carousel is active
    */
   function wchcarousel_get_is_active(): bool
   {
      $is_active = get_option("wchcarousel_active");

      return $is_active;
   }
}

if (!function_exists("wchcarousel_get_image_count")) {
   /**
    * Get the current number of images in the carousel
    *
    * @return int the number of images in the carousel
    */
   function wchcarousel_get_image_count(): int
   {
      $list = get_option("wchcarousel_list");

      return $list ? count($list) : 0;
   }
}

if (!function_exists("wchcarousel_set_is_active")) {
   /**
    * Set the active state of the carousel
    *
    * @param bool $is_active whether the carousel is active or not
    * @return void
    */
   function wchcarousel_set_is_active(bool $is_active): void
   {
      update_option("wchcarousel_active", $is_active);
   }
}

if (!function_exists("wchcarousel_set_images")) {
   /**
    * Set the carousel images to the current collection
    *
    * @param array $images the images to set for the carousel
    * @return void
    */
   function wchcarousel_set_images(array $images): void
   {
      update_option("wchcarousel_list", $images);
   }
}

if (!function_exists("wchcarousel_images_get")) {
   /**
    * AJAX endpoint for getting list of images
    *
    * @return void
    */
   function wchcarousel_images_get(): void
   {
      $entries = array();
      $attachments = new WP_Query(array(
         "post_type" => "attachment",
         "post_status" => "inherit"
      ));

      foreach ($attachments->posts as $image) {
         array_push($entries, new wchcarousel_wpimage(
            $image->ID,
            $image->post_title,
            wp_get_attachment_url($image->ID)
         ));
      }
      
      echo json_encode($entries);

      wp_die();
   }
}

if (!function_exists("wchcarousel_carousel_images_get")) {
   /**
    * AJAX endpoint for getting current carousel images
    *
    * @return void
    */
   function wchcarousel_carousel_images_get(): void
   {
      $list = get_option("wchcarousel_list");

      echo json_encode($list);

      wp_die();
   }
}

if (!function_exists("wchcarousel_image_submit")) {
   /**
    * Post endpoint for image submission
    *
    * @return void
    */
   function wchcarousel_image_submit(): void
   {
      $image_selection_prefix = "wchcarousel_admin_image_selection_";
      $image_caption_prefix = "wchcarousel_admin_image_caption_";

      $is_active = array_key_exists("is_active", $_POST);
      $images = array();

      for ($i = 0; array_key_exists(($image_selection_prefix.strval($i)), $_POST); $i++)
         array_push($images, new wchcarousel_image(
            $_POST[$image_selection_prefix.strval($i)],
            $_POST[$image_caption_prefix.strval($i)]
         ));

      wchcarousel_set_is_active($is_active);
      wchcarousel_set_images($images);

      wp_safe_redirect($_SERVER["HTTP_REFERER"]);
      exit();
   }
}

if (!function_exists("wchcarousel_carousel_get")) {
   /**
    * Add carousel after <header> element
    *
    * @return void
    */
   function wchcarousel_carousel_get(): void
   {
      include plugin_dir_path(__FILE__)."templates/carousel.php";

      wp_die();
   }
}

if (!function_exists("wchcarousel_setup")) {
   /**
    * Perform plugin setup.
    * 
    * @return void
    */
   function wchcarousel_setup(): void
   {
      register_activation_hook(__FILE__, "wchcarousel_activate");
      register_deactivation_hook(__FILE__, "wchcarousel_deactivate");
      register_uninstall_hook(__FILE__, "wchcarousel_uninstall");

      add_action("admin_enqueue_scripts", "wchcarousel_styles");
      add_action("admin_enqueue_scripts", "wchcarousel_scripts");
      add_action("wp_enqueue_scripts", "wchcarousel_scripts");
      add_action("admin_menu", "wchcarousel_admin_menu");
      add_action("admin_post_image_submit", "wchcarousel_image_submit");
      add_action("wp_ajax_images_get", "wchcarousel_images_get");
      add_action("wp_ajax_carousel_images_get", "wchcarousel_carousel_images_get");
      add_action("wp_ajax_carousel_get", "wchcarousel_carousel_get");
   }

   wchcarousel_setup();
 }
document.addEventListener("DOMContentLoaded", () => {
    if (window.location.pathname === "/") {
        const main = document.getElementsByTagName("main");

        if (main && main[0]) {
            const request = new XMLHttpRequest();

            request.open("get", "/wp-admin/admin-ajax.php?action=carousel_get", true);
            request.addEventListener("readystatechange", () => {
                if (request.readyState === 4) {
                    if (request.status === 200) {
                        document.insertBefore(main[0], document.createRange().createContextualFragment(request.responseText));
                    }
                }
            })
            request.send();
        }
    }
});
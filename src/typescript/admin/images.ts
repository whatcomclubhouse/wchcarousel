document.addEventListener("DOMContentLoaded", () => {
    const form = document.getElementById("wchcarousel_admin_form") as HTMLFormElement;

    if (form) {
        const count_input = document.getElementById("wchcarousel_admin_image_count") as HTMLInputElement;

        redraw_image_sections();

        count_input.addEventListener("change", () => {
            redraw_image_sections();
        });
    }
});

async function redraw_image_sections(): Promise<void> {
    const images_request = new XMLHttpRequest();
    const current_images_request = new XMLHttpRequest();

    const count_input = document.getElementById("wchcarousel_admin_image_count") as HTMLInputElement;
    const div = document.getElementById("wchcarousel_admin_images") as HTMLDivElement;

    const images_promise: Promise<string> = new Promise((resolve, reject) => {
        images_request.open("get", "admin-ajax.php?action=images_get", true);
        images_request.addEventListener("readystatechange", () => {
            if (images_request.readyState === 4) {
                if (images_request.status === 200) {
                    resolve(images_request.response);
                } else {
                    reject();
                }
            }
        });
        images_request.send();
    });

    const current_images_promise: Promise<string> = new Promise((resolve, reject) => {
        current_images_request.open("get", "admin-ajax.php?action=carousel_images_get", true);
        current_images_request.addEventListener("readystatechange", () => {
            if (current_images_request.readyState === 4) {
                if (current_images_request.status === 200) {
                    resolve(current_images_request.response);
                } else {
                    reject();
                }
            }
        });
        current_images_request.send();
    });

    let images: any[] = [];
    let current_images: any[] = [];

    await Promise.all([images_promise, current_images_promise])
        .then((values) => {
            images = JSON.parse(values[0]);
            current_images = JSON.parse(values[1]);
        });

    while(div.hasChildNodes())
        div.removeChild(div.lastChild as ChildNode);

    for (let i = 0; i < +count_input.value; i++) {
        const container = document.createElement("div");
        const img_preview = document.createElement("div");
        const dropdown_group = document.createElement("div");
        const dropdown = document.createElement("select");
        const dropdown_span = document.createElement("span");
        const dropdown_default = document.createElement("option");
        const img = document.createElement("img");
        const caption_setter = document.createElement("div");
        const span = document.createElement("span");
        const textarea = document.createElement("textarea");

        dropdown_default.selected = true;
        dropdown_default.innerText = "Select an image";
        dropdown_default.value = "";
        dropdown.appendChild(dropdown_default);

        for (const image of images) {
            const option = document.createElement("option");

            option.value = image.attachment_id;
            option.innerText = image.name;

            dropdown.appendChild(option);
        }

        dropdown_span.innerText = "Image";
        dropdown_span.classList.add("input-group-text");

        dropdown.classList.add("form-control");
        dropdown.required = true;
        dropdown.id = "wchcarousel_admin_image_selection_" + i.toString();
        dropdown.name = dropdown.id;

        dropdown_group.classList.add("input-group");
        dropdown_group.appendChild(dropdown_span);
        dropdown_group.appendChild(dropdown);

        img_preview.appendChild(dropdown_group);
        img_preview.appendChild(img);

        span.innerText = "Caption";
        span.classList.add("input-group-text");
        textarea.classList.add("form-control");
        textarea.id = "wchcarousel_admin_image_caption_" + i.toString();
        textarea.name = textarea.id;

        caption_setter.classList.add("input-group");
        caption_setter.appendChild(span);
        caption_setter.appendChild(textarea);

        container.classList.add("mb-3");
        container.appendChild(img_preview);
        container.appendChild(caption_setter);

        div.appendChild(container);

        if (i < current_images.length) {
            dropdown.value = current_images[i].attachment_id;
            textarea.value = current_images[i].caption;
        }
    }
}